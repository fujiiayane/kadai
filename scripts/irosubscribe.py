#!/usr/bin/env python
#coding: UTF-8
import rospy
from std_msgs.msg import String

def callback(message):
	iro = {"りんご":"赤","ぶどう":"紫","ばなな":"黄","みかん":"オレンジ","もも":"ピンク"}
	val = iro[message.data]
	print(val)

rospy.init_node('irosubscribe')
sub = rospy.Subscriber('iro', String, callback)
rospy.spin()
