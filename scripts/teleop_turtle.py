#!/usr/bin/env python
#coding: UTF-8
import rospy
from geometry_msgs.msg import Twist

rospy.init_node('teleop_turtle')
pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size = 10)
while not rospy.is_shutdown():
	vel = Twist()
	direction = raw_input('前, 後ろ, 左, 右 >')
	if '前' in direction:
		vel.linear.x = 0.5
	elif '後ろ' in direction:
		vel.linear.x = -0.5
	elif '左' in direction:
		vel.angular.z = 1.0
	elif '右' in direction:
		vel.angular.z = -1.0
	elif '終了' in direction:
		break
	print(vel)
	pub.publish(vel)
